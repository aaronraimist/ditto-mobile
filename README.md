Build status on App Center
[![Build status](https://build.appcenter.ms/v0.1/apps/7fc3d3ff-4783-4ad1-b977-58f8b3719022/branches/master/badge)](https://appcenter.ms)

# Ditto Chat

![](http://dittochat.org/wp-content/uploads/2019/10/preview-1024x1005.png)

## Get Ditto

- **iOS** - [Testflight Link](https://testflight.apple.com/join/9M0ERlKd)
- **Android** - TBD

## Running Locally

Prerequisite: React Native, watchman, yarn, cocoapods, XCode Command Line Tools

1. Clone the project
2. Run `yarn` in root directory
3. Open terminal and run `yarn start`
4. Open another terminal and run `yarn ios` for iOS and `yarn and` for Android

Note: `yarn ios` runs iPhone Xs by default - you may need to change this depending on what simulators you have installed.

## Troubleshooting

If the project is crashing and you don't know why, try running `yarn reset`, and then follow steps 3 and 4 above.

Still having trouble? Email me at `annie@elequin.io` or message me on Matrix at `@annie:elequin.io`
