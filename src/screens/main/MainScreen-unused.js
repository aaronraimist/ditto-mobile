import React, {Component} from 'react';
import {connect} from 'react-redux';
import PageWrapper from '../../components/library/PageWrapper';
import Image from 'react-native-scalable-image';
import styled from 'styled-components/native';
import BottomSheet from '../../components/BottomSheet';
import {GiftedChat} from 'react-native-gifted-chat';
import CustomGiftedChat from '../../components/CustomGiftedChat';
import {SCREEN_WIDTH, COLORS} from '../../constants';
import {
  currentRoomSelector,
  userDataSelector,
  clientSelector,
  newMessageSelector,
} from '../../redux/selectors';
import _ from 'lodash';
import {isIphoneX} from '../../utilities/isIphoneX';
import {
  retrieveMessageListFromCache,
  saveMessageListToCache,
} from '../../api/matrix';
import {updateNewMessage} from '../../redux/actions/MatrixActions';

const backgroundBlob = require('../../assets/images/blob6.png');

const debug = require('debug')('ditto:screen:MainScreen');

type IProps = {
  //
};

class MainScreen extends Component {
  state = {
    messages: [],
  };

  componentDidMount = () => {
    // this.setState({messages: this.formatMessages()});
    this.getMessages();
  };

  componentDidUpdate = async prevProps => {
    if (!_.isEqual(prevProps.currentRoom, this.props.currentRoom)) {
      debug('not equal');
      this.getMessages();
    }
    if (!prevProps.newMessage && this.props.newMessage) {
      this.props.updateNewMessage(false);
      this.getMessages();
    }
  };

  getMessages = async () => {
    const {currentRoom} = this.props;
    if (!currentRoom) return [];
    try {
      const newMessages = currentRoom?.getLiveTimeline().getEvents();
      debug('got live messages');
      saveMessageListToCache(currentRoom.roomId, newMessages);
      this.setState({messages: []}, () => {
        this.setState({messages: this.formatMessages(newMessages || null)});
      });
    } catch (err) {
      debug('Getting messages from cache');
      debug('current room: ', currentRoom);
      const cachedMessages = await retrieveMessageListFromCache(
        currentRoom.roomId,
      );
      debug('cached messages: ', cachedMessages);
      this.setState({messages: []}, () => {
        this.setState({messages: this.formatMessages(cachedMessages, true)});
      });
    }
  };

  getMessageType = msg => {
    let msgType = '';
    try {
      msgType = msg.getType();
    } catch {
      msgType = msg.type;
    }
    return msgType;
  };

  getMessageContent = msg => {
    let msgContent = '';
    try {
      msgContent = msg.getContent();
    } catch {
      msgContent = msg.content;
    }
    return msgContent;
  };

  formatMessages = (messages, cached = false) => {
    if (!messages) return [];
    const formattedMessages = [];

    messages.forEach(msg => {
      if (
        this.getMessageType(msg) === 'm.room.message' &&
        this.getMessageContent(msg).msgtype === 'm.text'
      ) {
        const newMsg = {
          _id: cached ? msg.event_id : msg.getId(),
          text: this.getMessageContent(msg).body,
          createdAt: cached ? msg.origin_server_ts : msg.getTs(),
          user: {
            _id: cached ? msg.sender : msg.getSender(),
            name: msg.sender.name,
          },
        };
        formattedMessages.push(newMsg);
      } else {
        if (
          this.getMessageType(msg) === 'm.room.member' &&
          this.getMessageContent(msg).membership === 'join'
        ) {
          const systemMsg = {
            _id: 1,
            text: `${this.getMessageContent(msg).displayname} has joined`,
            createdAt: cached ? msg.origin_server_ts : msg.getTs(),
            system: true,
          };
          formattedMessages.push(systemMsg);
        }
      }
    });
    debug('formatted', formattedMessages);
    return _.uniqBy(formattedMessages, item => item._id).reverse();
  };

  onSend = newMessages => {
    debug('new messages', newMessages);
    const {client, currentRoom} = this.props;

    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, newMessages),
    }));

    var content = {
      body: newMessages[0].text,
      msgtype: 'm.text',
    };

    client
      .sendEvent(currentRoom.roomId, 'm.room.message', content, '')
      .then(res => {
        debug('Message sent successfully', res);
      })
      .catch(err => {
        debug('Message could not be send: ', err);
      });
  };

  render() {
    return (
      <PageWrapper safeArea={false}>
        <BackgroundBlob source={backgroundBlob} />
        <Header>
          <HeaderText>{this.props.currentRoom?.name}</HeaderText>
        </Header>
        <CustomGiftedChat
          messages={this.state.messages}
          user={{_id: this.props.userData?.userId}}
          onSend={this.onSend}
        />
        <Spacer />
        <BottomSheet />
      </PageWrapper>
    );
  }
}
const mapStateToProps = state => {
  debug('App State', state);
  return {
    userData: userDataSelector(state),
    currentRoom: currentRoomSelector(state),
    client: clientSelector(state),
    newMessage: newMessageSelector(state),
  };
};
const mapDispatchToProps = {
  updateNewMessage,
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MainScreen);

const BackgroundBlob = styled(Image)`
  opacity: 0.1;
  position: absolute;
  top: -280;
  left: -350;
`;

const Header = styled.SafeAreaView`
  background-color: ${COLORS.gray.five};
  width: 100%;
  height: 80;
  z-index: 1;
  /* box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.2); */
  justify-content: center;
  align-items: center;
`;

const HeaderText = styled.Text`
  color: #fff;
  font-size: 18;
  font-weight: bold;
`;

const Spacer = styled.SafeAreaView`
  width: ${SCREEN_WIDTH};
  height: ${isIphoneX() ? 140 : 120};
  /* background-color: pink; */
`;
