import React, {Component} from 'react';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/Entypo';
import styled from 'styled-components/native';
import {COLORS} from '../../constants';
import {GiftedChat} from 'react-native-gifted-chat';
import {
  userDataSelector,
  clientSelector,
  currentMessagesSelector
} from '../../redux/selectors';
import CustomGiftedChat from '../../components/CustomGiftedChat';
import {setCurrentMessages} from '../../redux/actions/MatrixActions';
import {isEqual} from 'lodash';
import {
  fetchPreviousMessages,
  getRoomTimeline,
  retrieveHomeserverFromCache
} from '../../api/matrix';

const debug = require('debug')('ditto:screens:ChatScreen');

class ChatScreen extends Component {
  static navigationOptions = ({navigation: nav}) => {
    const room = nav.getParam('room', 'Room');
    debug('room', room);
    return {
      title: room.name,
      headerTitleStyle: {
        color: COLORS.gray.one
      },
      headerLeft: <BackButton onPress={() => nav.goBack()} />,
      headerRight: null
    };
  };

  state = {
    homeserver: '',
    messages: [],
    lastToken: ''
  };

  formatForGiftedChat = () => {
    const {messages} = this.props;
    const formatted = [];
    messages.forEach(msg => {
      if (msg.isTempMessage) {
        formatted.push(msg);
      } else if (msg.content?.msgtype === 'm.text') {
        const newMessage = {
          _id: msg.event_id,
          text: msg.content.body,
          createdAt: msg.origin_server_ts,
          user: {
            _id: msg.sender,
            name: msg.sender
          },
          ...msg
        };
        formatted.push(newMessage);
      } else if (msg.type === 'm.room.name') {
        const room = this.props.navigation.getParam('room', null);
        room.name = msg.content.name;
        this.props.navigation.setParams({room});
        const newMessage = {
          _id: msg.event_id,
          text: `${msg.sender} changed the room name to ${msg.content.name}`,
          createdAt: msg.origin_server_ts,
          system: true,
          ...msg
        };
        formatted.push(newMessage);
      } else if (msg.type === 'm.room.member') {
        if (msg.content.membership === 'join') {
          const newMessage = {
            _id: msg.event_id,
            text: `${msg.content.displayname} has joined`,
            createdAt: msg.origin_server_ts,
            system: true,
            ...msg
          };
          formatted.push(newMessage);
        }
      } else if (msg.content?.msgtype === 'm.image') {
        const mxc = msg.content.url;
        const pieces = mxc.split('/');
        const imageUrl = `${this.state.homeserver}/_matrix/media/r0/download/${pieces[2]}/${pieces[3]}`;
        debug('imageUrl: ', imageUrl);
        const newMessage = {
          _id: msg.event_id,
          text: 'My message',
          createdAt: msg.origin_server_ts,
          user: {
            _id: msg.sender,
            name: msg.sender
          },
          image: imageUrl
        };
        formatted.push(newMessage);
      }
    });
    formatted.sort((a, b) => b.createdAt - a.createdAt);
    this.setState({messages: formatted});
    return formatted;
  };

  onSend = newMessages => {
    debug('new messages', newMessages);
    debug('my user id: ', this.props.userData.userId);

    var content = {
      body: newMessages[0].text,
      msgtype: 'm.text'
    };

    const _id = Math.random() * 10000;
    const newMsg = {
      ...content,
      isTempMessage: true,
      _id,
      event_id: _id,
      text: newMessages[0].text,
      createdAt: new Date(),
      user: {
        _id: this.props.userData.userId
      }
    };

    this.props.setCurrentMessages([...this.props.messages, newMsg]);

    const currentRoom = this.props.navigation.getParam('room', null);
    this.props.client
      .sendEvent(currentRoom.roomId, 'm.room.message', content, '')
      .then(res => {
        debug('Message sent successfully', res);
      })
      .catch(err => {
        debug('Message could not be send: ', err);
      });
  };

  loadEarlierMessages = async () => {
    // debug('LOAD EARLIER MESSAGES');
    const room = this.props.navigation.getParam('room', null);
    const response = await fetchPreviousMessages(
      room.roomId,
      this.state.lastToken
    );
    this.setState({lastToken: response.data.end});
    this.props.setCurrentMessages([
      ...response.data.chunk,
      ...this.props.messages
    ]);
  };

  //********************************************************************************
  // Lifecycle
  //********************************************************************************

  componentDidMount = async () => {
    const room = this.props.navigation.getParam('room', null);
    // const messages = room.getLiveTimeline().getEvents();
    const messages = getRoomTimeline(room);
    debug('messages', messages);
    this.props.setCurrentMessages(messages);

    const homeserver = await retrieveHomeserverFromCache();
    this.setState({homeserver}, () => {
      this.setState({messages: this.formatForGiftedChat()});
    });

    const response = await fetchPreviousMessages(room.roomId);
    // debug('last tokENNNN', response);
    this.setState({lastToken: response.data.end});
  };

  componentDidUpdate = ({messages}) => {
    if (!isEqual(messages, this.props.messages)) {
      this.setState({messages: this.formatForGiftedChat()});
    }
  };

  componentWillUnmount = () => {
    this.props.setCurrentMessages([]);
  };

  render() {
    // debug('this.props.messages', this.props.messages);
    return (
      <Wrapper>
        <CustomGiftedChat
          messages={this.state.messages}
          onSend={this.onSend}
          user={{
            _id: this.props.userData.userId
          }}
          onEndReached={this.loadEarlierMessages}
        />
      </Wrapper>
    );
  }
}
const mapStateToProps = state => ({
  userData: userDataSelector(state),
  client: clientSelector(state),
  messages: currentMessagesSelector(state)
});
const mapDispatchToProps = {
  setCurrentMessages
};
export default connect(mapStateToProps, mapDispatchToProps)(ChatScreen);

const BackTouchable = styled.TouchableOpacity``;

const BackButton = ({onPress}) => (
  <BackTouchable onPress={onPress}>
    <Icon
      name='chevron-left'
      size={35}
      color={COLORS.gray.one}
      style={{marginLeft: 4}}
    />
  </BackTouchable>
);

const Wrapper = styled.View`
  flex: 1;
  background-color: ${COLORS.dark};
`;
