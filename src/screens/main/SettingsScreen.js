import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Linking} from 'react-native';
import PageWrapper from '../../components/library/PageWrapper';
import styled from 'styled-components/native';
import Button from '../../components/library/Button';
// import DittoIcon from '../../assets/icons/DittoIcon';
import {logOutUser} from '../../redux/actions/MatrixActions';
import {clientSelector, userDataSelector} from '../../redux/selectors';
import TextButton from '../../components/library/TextButton';
import {retrieveHomeserverFromCache} from '../../api/matrix';
import BackIcon from '../../assets/icons/icon-chevron-left.svg';

var codePush = require('react-native-code-push');
const debug = require('debug')('ditto:screen:SettingsScreen');

class SettingsScreen extends Component {
  state = {
    displayName: '',
    avatarUrl: ''
  };

  componentDidMount = async () => {
    const {client, userId} = this.props;

    debug('client', client);
    debug('userId', userId);
    const homeserver = await retrieveHomeserverFromCache();
    const {displayname} = await client.getProfileInfo(userId, 'displayname');
    const avatarData = await client.getProfileInfo(userId, 'avatar_url');
    debug('avatar data', avatarData.avatar_url.split('/'));
    const avatarId = avatarData.avatar_url.split('/')[3];
    const avatarUrl = `${homeserver ||
      'https://matrix.org'}/_matrix/media/r0/download/elequin.io/${avatarId}?width=150&height=150&method=crop`;

    this.setState({displayName: displayname, avatarUrl});
  };

  goBack = () => this.props.navigation.goBack();

  logOut = () => {
    this.props.logOutUser();
    this.props.navigation.navigate('Auth');
  };

  openFeatureRequests = () => {
    const url = 'https://ditto.upvoty.com/b/feature-requests/';
    this.openUrl(url);
  };

  openBugReports = () => {
    const url = 'https://ditto.upvoty.com/b/bug-reports/';
    this.openUrl(url);
  };

  openUrl = url => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        debug("Don't know how to open URL: " + url);
      }
    });
  };

  render() {
    return (
      <PageWrapper alignCenter safeArea>
        <BackButton onPress={this.goBack}>
          <BackIcon name='chevron-left' />
        </BackButton>
        <Avatar source={{uri: this.state.avatarUrl}} />
        <NameTitle>{this.state.displayName}</NameTitle>
        <UserId>{this.props.userId}</UserId>
        <Feedback>Please submit feedback!</Feedback>
        <FeedbackWrapper>
          <TextButton size='md' onPress={this.openFeatureRequests}>
            Feature Requests
          </TextButton>
          <TextButton size='md' onPress={this.openBugReports}>
            Bug Reports
          </TextButton>
        </FeedbackWrapper>
        <ButtonWrapper>
          <Button title='Log Out' onPress={this.logOut} />
        </ButtonWrapper>
      </PageWrapper>
    );
  }
}
const mapStateToProps = state => ({
  client: clientSelector(state),
  userId: userDataSelector(state)?.userId
});
const mapDispatchToProps = {
  logOutUser
};
export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen);

const BackButton = styled.TouchableOpacity`
  padding: 20px;
  align-self: flex-start;
`;

// const BackIcon = styled(DittoIcon)`
//   color: ${({theme}) => theme.dittoWhite};
//   font-size: 30;
// `;

const Avatar = styled.Image`
  width: 150;
  height: 150;
  background-color: gray;
  border-radius: 80;
`;

const NameTitle = styled.Text`
  color: ${({theme}) => theme.dittoWhite};
  font-size: 25;
  font-weight: 700;
  margin-top: 20;
`;

const UserId = styled.Text`
  color: ${({theme}) => theme.dittoWhite};
  opacity: 0.4;
  margin-top: 5;
`;

const ButtonWrapper = styled.View`
  margin-top: 50;
`;

const Feedback = styled.Text`
  color: ${({theme}) => theme.dittoWhite};
  font-size: 20;
  margin-top: 50;
`;

const FeedbackWrapper = styled.View`
  margin-top: 10;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 100%;
`;
