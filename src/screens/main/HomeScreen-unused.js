import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Text, View} from 'react-native';
import styled from 'styled-components/native';
import {userDataSelector, roomListSelector} from '../../redux/selectors';
import {getChatSnippet} from '../../utilities/Misc';
import UserHeader from '../../components/home/UserHeader';
import HeaderRight from '../../components/home/HeaderRight';
import { COLORS } from '../../constants';

const debug = require('debug')('ditto:screen:HomeScreen');

type IProps = {
  userData: any,
  rooms: any[]
};

type IState = {
  //
};

class HomeScreen extends Component<IProps, IState> {
  static navigationOptions = ({navigation}) => {
    return {
      headerLeft: <UserHeader />,
      headerRight: <HeaderRight />
    };
  };

  navigateToChat = () => {
    this.props.navigation.navigate('Chat');
  };

  render() {
    // debug('rooms', this.props.rooms);
    return (
      <Wrapper>
        {this.props.rooms.map(room => (
          <ChatListItem key={room.roomId} onPress={this.navigateToChat}>
            <Avatar />
            <ChatTitle>
              <Title numberOfLines={1}>{room.name}</Title>
              <Snippet numberOfLines={1}>{getChatSnippet(room)}</Snippet>
            </ChatTitle>
          </ChatListItem>
        ))}
      </Wrapper>
    );
  }
}
const mapStateToProps = state => {
  debug('App State', state);
  return {
    userData: userDataSelector(state),
    rooms: roomListSelector(state)
  };
};
const mapDispatchToProps = {
  //   updateNewMessage
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

const Wrapper = styled.ScrollView`
  flex: 1;
  background-color: #2b2148;
`;

const ChatListItem = styled.TouchableOpacity`
  padding-left: 14;
  padding-right: 14;
  padding-top: 10;
  padding-bottom: 10;
  flex-direction: row;
  align-items: center;
  margin-top: 0.4;
  margin-bottom: 0.4;
  background-color: ${COLORS.dark};
`;

const Avatar = styled.View`
  width: 50;
  height: 50;
  border-radius: 40;
  background-color: rgba(255, 255, 255, 0.4);
`;

const ChatTitle = styled.View`
  width: 300;
  margin-left: 14;
`;

const Title = styled.Text`
  font-weight: bold;
  font-size: 16;
  margin-bottom: 4;
  color: #fff;
`;

const Snippet = styled.Text`
  color: rgba(255, 255, 255, 0.4);
`;
