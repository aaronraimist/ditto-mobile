import React from 'react';
import styled from 'styled-components/native';
import ChatList from '../../../components/home/ChatList';

const debug = require('debug')('ditto:screen:DirectMessageScreen');

const GroupMessageScreen = () => (
  <Wrapper>
    <ChatList groups />
  </Wrapper>
);
export default GroupMessageScreen;

const Wrapper = styled.View`
  flex: 1;
  background-color: #241a39;
`;
