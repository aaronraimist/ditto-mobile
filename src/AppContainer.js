import React, {Component} from 'react';
import AppNavigator from './AppNavigator';
import {ThemeProvider} from 'styled-components';
import {connect} from 'react-redux';
import theme from './theme';
import { startupNotifications, shutdownNotifications } from './utilities/Notifications';

const debug = require('debug')('ditto:AppContainer');

class AppContainer extends Component {
  componentDidMount = () => {
    startupNotifications();
  }
  componentWillUnmount = () => {
    shutdownNotifications();
  }
  render() {
    return (
      <ThemeProvider theme={theme}>
        <AppNavigator />
      </ThemeProvider>
    );
  }
}
export default AppContainer;
