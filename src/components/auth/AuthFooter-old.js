import React from 'react';
import styled from 'styled-components/native';
import TextButton from '../library/TextButton';
import {responsive} from '../../utilities/ResponsiveValue';

const SubText = styled.View`
  position: absolute;
  bottom: ${responsive(20)};
  align-items: center;
`;

const TouchableWrapper = styled.TouchableOpacity`
  padding: 5px 20px;
`;

const PolicyText = styled.Text`
  color: ${({dark}) => (dark ? '#25114b' : '#fff')};
  font-weight: 200;
`;

const PrivacyPolicy = ({dark = false}) => (
  <TouchableWrapper>
    <PolicyText dark={dark}>Privacy Policy</PolicyText>
  </TouchableWrapper>
);

const AuthFooter = ({
  dark = false,
  showRegister = false,
  onPressTopButton = () => {},
  onPressPrivacyPolicy = () => {},
}) => (
  <SubText>
    <TextButton color={dark ? '#25114b' : '#fff'} onPress={onPressTopButton}>
      {showRegister ? 'Register?' : 'Login?'}
    </TextButton>
    <PrivacyPolicy dark={dark} />
  </SubText>
);
export default AuthFooter;
