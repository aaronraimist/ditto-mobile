import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Input from '../library/Input';
import styled from 'styled-components/native';
import {SCREEN_WIDTH} from '../../constants';

export default class SecondSignUpForm extends Component {
  usernameInput = null;
  newPasswordInput = null;
  confirmPasswordInput = null;

  focusNewPasswordInput = () => {
    this.usernameInput.blur();
    this.newPasswordInput.focus();
  };

  focusConfirmPasswordInput = () => {
    this.newPasswordInput.blur();
    this.confirmPasswordInput.focus();
  };

  render() {
    return (
      <Wrapper>
        <Input
          ref={ref => (this.usernameInput = ref)}
          marginValue="15px 0"
          placeholder="Username"
          returnKeyType="next"
          onSubmitEditing={this.focusNewPasswordInput}
        />
        <Input
          ref={ref => (this.newPasswordInput = ref)}
          secureTextEntry
          marginValue="0 0 15px"
          placeholder="New Password"
          returnKeyType="next"
          onSubmitEditing={this.focusConfirmPasswordInput}
        />
        <Input
          ref={ref => (this.confirmPasswordInput = ref)}
          secureTextEntry
          marginValue="0 0 15px"
          placeholder="Confirm Password"
          returnKeyType="done"
        />
      </Wrapper>
    );
  }
}

const Wrapper = styled.View`
  width: ${SCREEN_WIDTH};
  padding: 0 20px;
`;
