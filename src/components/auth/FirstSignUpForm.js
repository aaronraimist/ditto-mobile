import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Input from '../library/Input';
import styled from 'styled-components/native';
import {SCREEN_WIDTH} from '../../constants';

export default class FirstSignUpForm extends Component {
  firstNameInput = null;
  lastNameInput = null;
  emailInput = null;

  focusLastNameInput = () => {
    this.firstNameInput.blur();
    this.lastNameInput.focus();
  };

  focusEmailInput = () => {
    this.lastNameInput.blur();
    this.emailInput.focus();
  };

  render() {
    return (
      <Wrapper>
        <Input
          ref={ref => (this.firstNameInput = ref)}
          marginValue="15px 0"
          placeholder="First Name"
          returnKeyType="next"
          onSubmitEditing={this.focusLastNameInput}
        />
        <Input
          ref={ref => (this.lastNameInput = ref)}
          marginValue="0 0 15px"
          placeholder="Last Name"
          returnKeyType="next"
          onSubmitEditing={this.focusEmailInput}
        />
        <Input
          ref={ref => (this.emailInput = ref)}
          marginValue="0 0 15px"
          placeholder="Email"
          returnKeyType="done"
        />
      </Wrapper>
    );
  }
}

const Wrapper = styled.View`
  width: ${SCREEN_WIDTH};
  padding: 0 20px;
`;
