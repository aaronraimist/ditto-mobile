import styled from 'styled-components/native';
import {COLORS} from '../../constants';
import {fetchAvatarForRoom} from '../../api/matrix';

export const ChatListItem = styled.TouchableOpacity`
  padding-left: 14;
  padding-right: 14;
  padding-top: 10;
  padding-bottom: 10;
  flex-direction: row;
  align-items: center;
  margin-top: 0.4;
  margin-bottom: 0.4;
  background-color: ${COLORS.dark};
`;

export const Avatar = styled.Image`
  width: 50;
  height: 50;
  border-radius: 40;
  background-color: rgba(255, 255, 255, 0.4);
`;

export const ChatTitle = styled.View`
  width: 300;
  margin-left: 14;
`;

export const Title = styled.Text`
  font-weight: bold;
  font-size: 16;
  margin-bottom: 4;
  color: #fff;
`;

export const Snippet = styled.Text`
  color: rgba(255, 255, 255, 0.4);
`;
