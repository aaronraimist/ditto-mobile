import React from 'react';
import ChevronLeft from '../../assets/icons/icon-chevron-left.svg';
import {COLORS} from '../../constants';

const BackIcon = () => <ChevronLeft fill='#fff' />;
export default BackIcon;
