import styled from 'styled-components/native';

const Text = styled.Text`
  color: ${({color, theme}) => (color ? color : theme.dittoWhite)};
  font-size: 24;
`;
export default Text;
