import React from 'react';
import styled from 'styled-components/native';
import {COLORS} from '../../../constants';
// import DittoIcon from '../../../assets/icons/DittoIcon';

const buttonSize = 30;
const ButtonWrapper = styled.TouchableOpacity`
  background-color: ${COLORS.blue};
  border-radius: 40;
  align-items: center;
  justify-content: center;
  width: ${buttonSize};
  height: ${buttonSize};
  opacity: ${({disabled}) => (disabled ? 0.5 : 1)};
  box-shadow: 0px 5px 6px rgba(0, 0, 0, 0.15);
`;

const HeaderButton = props => (
  <ButtonWrapper>
    {/* <DittoIcon color={COLORS.gray.one} {...props} /> */}
  </ButtonWrapper>
);
export default HeaderButton;
