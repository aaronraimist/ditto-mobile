import React, {Component} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components/native';
import AvatarPlaceholder from './AvatarPlaceholder';
import {SCREEN_WIDTH, COLORS} from '../../../constants';
import HeaderAvatar from './HeaderAvatar';
import HeaderButton from './HeaderButton';
import {withNavigation} from 'react-navigation';
import {roomListSelector, currentRoomSelector} from '../../../redux/selectors';
import {setCurrentRoom} from '../../../redux/actions/MatrixActions';

class PanelHeader extends Component {
  navToSettings = () => this.props.navigation.navigate('Settings');
  render() {
    return (
      <>
        <Handle />
        <HeaderBubbles horizontal={true}>
          {this.props.rooms.map(room => {
            const showBadge = room?._notificationCounts?.total > 0;
            return (
              <HeaderAvatar
                key={room.roomId}
                onPress={() => this.props.setCurrentRoom(room)}
                showBadge={showBadge}
                currentRoom={room.roomId === this.props.currentRoom?.roomId}
                roomName={room.name.split(' ')[0]}
              />
            );
          })}
        </HeaderBubbles>
        <InboxHeader>
          <HeaderButton name="gear" size={18} onPress={this.navToSettings} />
          <Title>Messages</Title>
          <HeaderButton name="plus" size={15} />
        </InboxHeader>
      </>
    );
  }
}
const mapStateToProps = state => ({
  rooms: roomListSelector(state),
  currentRoom: currentRoomSelector(state),
});
const mapDispatchToProps = {
  setCurrentRoom,
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withNavigation(PanelHeader));

const Title = styled.Text`
  color: #fff;
  font-size: 30;
  font-weight: bold;
  margin: 15px 20px;
`;

const InboxHeader = styled.View`
  padding-top: 25;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const Handle = styled.View`
  background-color: ${COLORS.gray.three};
  height: 3;
  width: 50;
  border-radius: 2;
  margin-top: 3;
`;

const HeaderBubbles = styled.ScrollView`
  padding-top: 12;
  padding-right: 12;
  padding-bottom: 12;
  padding-left: 12;
  width: ${SCREEN_WIDTH};
`;
