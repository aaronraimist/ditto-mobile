import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View} from 'react-native';
import styled from 'styled-components/native';
import {COLORS} from '../../../constants';
import {roomListSelector, userDataSelector} from '../../../redux/selectors';
import ListAvatar from './ListAvatar';
import moment from 'moment';
import {setCurrentRoom} from '../../../redux/actions/MatrixActions';

const debug = require('debug')('ditto:component:PanelContent');

const defaultAvatar =
  'https://i2.wp.com/www.ahfirstaid.org/wp-content/uploads/2014/07/avatar-placeholder.png?fit=204%2C204';

class PanelContent extends Component {
  render() {
    debug('props: ', this.props);
    return (
      <>
        {this.props.rooms.map(room => {
          try {
            const timeline = room.getLiveTimeline().getEvents();

            const lastMessage = timeline[timeline.length - 1];
            const msgContent = lastMessage?.event?.content;

            const snippet =
              msgContent?.msgtype === 'm.text'
                ? msgContent?.body
                : 'Something happened!';

            const speaker = lastMessage?.sender?.name || null;

            const ageInMilliseconds = lastMessage?.event?.unsigned?.age;
            const ageInSeconds = ageInMilliseconds / 1000;
            const duration = moment.duration(`00:00:${ageInSeconds}`);
            const timeOfEvent = moment().subtract(duration);
            return (
              <InboxItem
                key={room.roomId}
                onPress={() => this.props.setCurrentRoom(room)}
                roomTitle={room.name}
                snippet={snippet}
                speaker={speaker}
                avatar={defaultAvatar}
                displayTime={timeOfEvent.fromNow()}
              />
            );
          } catch {
            // Reading rooms from cache
            return (
              <InboxItem
                key={room.roomId}
                onPress={() => this.props.setCurrentRoom(room)}
                roomTitle={room.name}
                snippet={''}
                speaker={''}
                avatar={defaultAvatar}
                displayTime={'--:--'}
              />
            );
          }
        })}
        <InboxFooter />
      </>
    );
  }
}
const mapStateToProps = state => ({
  rooms: roomListSelector(state),
  accessToken: userDataSelector(state)?.accessToken,
});
const mapDispatchToProps = {
  setCurrentRoom,
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PanelContent);

const Name = styled.Text`
  font-weight: 500;
  color: white;
  font-size: 20;
  width: 220;
`;

const ItemText = styled.View`
  margin-left: 8;
`;

const Snippet = styled.Text`
  color: white;
  font-size: 14;
  width: 220;
  opacity: 0.4;
`;

const InboxItemWrapper = styled.TouchableHighlight`
  background-color: ${COLORS.gray.five};
  flex-direction: row;
  align-items: center;
  padding: 5px 15px;
`;

const SenderName = styled(Snippet)`
  font-weight: bold;
  opacity: 1;
`;

const Time = styled.Text`
  font-size: 12;
  width: 50;
  text-align: right;
  color: ${({theme}) => theme.dittoWhite};
  opacity: 0.5;
`;

const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

const InboxItem = ({
  roomTitle,
  snippet,
  speaker,
  avatar,
  displayTime,
  onPress,
}) => (
  <InboxItemWrapper underlayColor={'#2c2234'} onPress={onPress}>
    <Row>
      <ListAvatar source={avatar} />
      <ItemText>
        <Name numberOfLines={1}>{roomTitle}</Name>
        <Snippet numberOfLines={1}>
          {speaker && <SenderName>{speaker?.split(' ')[0]}: </SenderName>}
          {snippet}
        </Snippet>
      </ItemText>
      <ItemText>
        <Time>{displayTime}</Time>
      </ItemText>
    </Row>
  </InboxItemWrapper>
);

const InboxFooter = styled.View`
  background-color: ${COLORS.gray.five};
  width: 100%;
  min-height: 100;
  height: 100%;
`;
