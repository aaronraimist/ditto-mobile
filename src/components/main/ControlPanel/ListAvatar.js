import React from 'react';
import styled from 'styled-components/native';
import Image from 'react-native-scalable-image';

const avatarSize = 50;

const AvatarWrapper = styled.TouchableOpacity`
  position: relative;
  width: ${avatarSize};
  height: ${avatarSize};
  margin-left: 5;
  margin-right: 5;
  margin-bottom: 10;
`;

const StyledImage = styled(Image)`
  border-radius: 50;
  background-color: gray;
  margin-top: 5;
`;

const ListAvatar = ({source}) => (
  <AvatarWrapper>
    <StyledImage source={{uri: source}} width={avatarSize} />
  </AvatarWrapper>
);
export default ListAvatar;
