import React from 'react';
import styled from 'styled-components/native';
import Image from 'react-native-scalable-image';
import {COLORS} from '../../../constants';

const faker = require('faker');

const defaultAvatar =
  'https://i2.wp.com/www.ahfirstaid.org/wp-content/uploads/2014/07/avatar-placeholder.png?fit=204%2C204';

const avatarSize = 50;

const AvatarWrapper = styled.TouchableOpacity`
  position: relative;
  width: ${avatarSize};
  height: ${avatarSize};
  margin-left: 5;
  margin-right: 5;
  margin-bottom: 10;
`;

const StyledImage = styled(Image)`
  border-radius: 50;
  background-color: gray;
  ${({currentRoom}) =>
    currentRoom ? `border-width: 3; border-color: ${COLORS.gray.two};` : ''}
`;

const badgeSize = 12;
const badgeOffset = 1;
const Badge = styled.View`
  width: ${badgeSize};
  height: ${badgeSize};
  border-radius: 50;
  position: absolute;
  top: ${badgeOffset};
  left: ${badgeOffset};
  background-color: ${COLORS.orange};
`;

const Name = styled.Text`
  text-align: center;
  width: ${avatarSize};
  color: ${({theme}) => theme.dittoWhite};
  font-size: 12;
  margin-top: 3;
`;

const HeaderAvatar = ({
  showBadge = false,
  roomName = 'Name',
  currentRoom = false,
  source = defaultAvatar,
  onPress = () => {},
}) => (
  <AvatarWrapper onPress={onPress}>
    <StyledImage
      source={{uri: source}}
      width={avatarSize}
      currentRoom={currentRoom}
    />
    <Name numberOfLines={1}>{roomName}</Name>
    {showBadge && <Badge />}
  </AvatarWrapper>
);
export default HeaderAvatar;
