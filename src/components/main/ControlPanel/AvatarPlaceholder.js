import styled from 'styled-components/native';

const avatarSize = 50;
const AvatarPlaceholder = styled.View`
  width: ${avatarSize};
  height: ${avatarSize};
  border-radius: 50;
  background-color: gray;
  margin: 5px;
`;
export default AvatarPlaceholder;
