const debug = require('debug')('ditto:utilities:Misc');

export const getChatSnippet = room => {
  try {
    const timeline = room.getLiveTimeline().getEvents();

    const lastMessage = timeline[timeline.length - 1];
    const msgContent = lastMessage?.event?.content;

    const snippet =
      msgContent?.msgtype === 'm.text'
        ? msgContent?.body
        : 'Something happened!';

    return snippet;
  } catch (error) {
    // debug('Error getting room timeline', error);
    return '...';
  }
};
