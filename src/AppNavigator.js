import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';

import AuthLoadingScreen from './screens/auth/AuthLoadingScreen';
import LandingScreen from './screens/auth/LandingScreen';
import LoginScreen from './screens/auth/LoginScreen';
import SignUpOneScreen from './screens/auth/SignUpOneScreen';
import SignUpTwoScreen from './screens/auth/SignUpTwoScreen';
// import MainScreen from './screens/main/MainScreen';
import SettingsScreen from './screens/main/SettingsScreen';
// import HomeScreen from './screens/main/HomeScreen';
import ChatScreen from './screens/main/ChatScreen';
import DirectMessageScreen from './screens/main/home/DirectMessageScreen';
import GroupMessageScreen from './screens/main/home/GroupMessageScreen';
import UserHeader from './components/home/UserHeader';
import HeaderRight from './components/home/HeaderRight';
import {COLORS} from './constants';
import {isIphoneX} from 'react-native-iphone-x-helper';

const HomeSwipeStack = createMaterialTopTabNavigator(
  {
    DirectMessages: {
      screen: DirectMessageScreen,
      navigationOptions: {
        tabBarLabel: 'Messages'
      }
    },
    GroupMessages: {
      screen: GroupMessageScreen,
      navigationOptions: {
        tabBarLabel: 'Groups'
      }
    }
  },
  {
    initialRouteName: 'DirectMessages',
    tabBarPosition: 'bottom',
    swipeEnabled: true,
    tabBarOptions: {
      activeTintColor: COLORS.gray.one,
      inactiveTintColor: '#898593',
      style: {
        backgroundColor: '#241A39',
        borderTopWidth: 0.4,
        borderTopColor: '#2F2646',
        height: isIphoneX() ? 75 : undefined,
        paddingLeft: 40,
        paddingRight: 40
      },
      indicatorStyle: {
        height: 0
      },
      tabStyle: {
        alignSelf: 'center',
        padding: 0
      },
      labelStyle: {
        fontWeight: 'bold',
        fontSize: 22
      },
      upperCaseLabel: false
    }
  }
);

const AppStack = createStackNavigator(
  {
    // Main: MainScreen,
    Home: HomeSwipeStack,
    Chat: ChatScreen,
    Settings: SettingsScreen
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#241A39',
        borderBottomWidth: 0.4,
        borderBottomColor: '#2F2646'
      },
      headerLeft: <UserHeader />,
      headerRight: <HeaderRight />
    }
    // headerMode: 'none',
  }
);

const AuthStack = createStackNavigator(
  {
    Landing: LandingScreen,
    SignUpOne: SignUpOneScreen,
    SignUpTwo: SignUpTwoScreen,
    Login: LoginScreen
  },
  {
    initialRouteName: 'Landing',
    headerMode: 'none'
  }
);

const AppNavigator = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    Auth: AuthStack,
    App: AppStack
  },
  {
    initialRouteName: 'AuthLoading',
    headerMode: 'none',
    navigationOptions: {
      gesturesEnabled: false
    }
  }
);

export default createAppContainer(AppNavigator);
