import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import RootReducer from './reducers';

type IUser = {
  userId: string,
  accessToken: string,
  displayName: string,
};

export type IMatrixState = {
  client: any, // the matrix client
  homeserver: string,
  currentMessages: any[],
  currentRoom: any,
  user: IUser,
  syncError: any,
  isSyncing: boolean,
};

export type IAppState = {
  matrix: IMatrixState,
  auth: IAuthState,
};

const store = createStore(RootReducer, applyMiddleware(thunk));
export default store;
