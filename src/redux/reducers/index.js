import {combineReducers} from 'redux';
import MatrixReducer from './MatrixReducer';

export default combineReducers({
  matrix: MatrixReducer,
});
