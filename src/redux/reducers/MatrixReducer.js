import {MatrixActionTypes} from '../actions/MatrixActions';
import {IMatrixState} from '../store';

const debug = require('debug')('ditto:redux:reducer:MatrixReducer');

const initialState: IMatrixState = {
  client: null,
  userData: null,
  authError: null,
  syncError: null,
  isSyncing: true,
  roomList: [],
  currentRoom: null,
  currentMessages: [],
  newMessage: false
};

const MatrixReducer = (state = initialState, action) => {
  debug('Action: ', action);
  switch (action.type) {
    case MatrixActionTypes.SET_MATRIX_CLIENT:
      return {...state, client: action.payload};
    case MatrixActionTypes.SET_AUTH_ERROR:
      return {...state, authError: action.payload};
    case MatrixActionTypes.CLEAR_AUTH_ERROR:
      return {...state, authError: null};
    case MatrixActionTypes.SET_SYNC_ERROR:
      return {...state, syncError: action.payload};
    case MatrixActionTypes.UPDATE_IS_SYNCING:
      return {...state, isSyncing: action.payload};
    case MatrixActionTypes.UPDATE_NEW_MSG:
      return {...state, newMessage: action.payload};
    case MatrixActionTypes.SET_USER_DATA:
      return {...state, userData: action.payload};
    case MatrixActionTypes.SET_ROOM_LIST:
      return {...state, roomList: action.payload};
    case MatrixActionTypes.SET_CURRENT_ROOM:
      return {...state, currentRoom: action.payload};
    case MatrixActionTypes.SET_CURRENT_MESSAGES:
      return {...state, currentMessages: action.payload};
    case MatrixActionTypes.USER_LOGOUT:
      return initialState;
    default:
      return state;
  }
};
export default MatrixReducer;
